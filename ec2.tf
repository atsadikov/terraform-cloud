provider "aws" {
    region = "us-west-2"
}

# Create a new instance of the latest Amazon Linux 2 AMI (HVM), SSD Volume Type on an
resource "aws_instance" "web1" {
  ami           = "ami-0a36eb8fadc976275"
  instance_type = "t2.micro"

  tags = {
    Name = "andrey-terraform-web1"
  }
}

output "eip" {
    value = aws_instance.web1.public_ip
}